%define gitcommit a52cc643cfe6733465cfc9ccb3d21cbdc4fd7506
%define gitdate 20190412
%define gitrev .%{gitdate}git%(c=%{gitcommit}; echo ${c:0:7})

%undefine _hardened_build

Name:           igt-gpu-tools
Version:        1.23
Release:        1%{?gitrev}%{?dist}
Summary:        Test suite and tools for DRM drivers

License:        MIT
URL:            https://gitlab.freedesktop.org/drm/igt-gpu-tools

%if 0%{?gitdate}
Source0:        igt-gpu-tools-%{gitdate}.tar.bz2
%else
Source0:        https://gitlab.freedesktop.org/drm/igt-gpu-tools/-/archive/igt-gpu-tools-%{version}/igt-gpu-tools-igt-gpu-tools-%{version}.tar.bz2
%endif
Source1:        make-git-snapshot.sh

# TODO FIXME TODO FIXME TODO
# Submit this upstream and put a comment on it's current upstream status
Patch0:         0001-lib-tests-Fix-test-failures-with-meson-0.50.0.patch

Provides:       xorg-x11-drv-intel-devel intel-gpu-tools
Obsoletes:      xorg-x11-drv-intel-devel intel-gpu-tools

BuildRequires:  meson
BuildRequires:  gcc
BuildRequires:  flex bison
BuildRequires:  pkgconfig(libdrm) >= 2.4.82
BuildRequires:  pkgconfig(pciaccess) >= 0.10
BuildRequires:  pkgconfig(libkmod)
BuildRequires:  pkgconfig(libprocps)
BuildRequires:  pkgconfig(libunwind)
BuildRequires:  pkgconfig(libdw)
BuildRequires:  pkgconfig(pixman-1)
BuildRequires:  pkgconfig(valgrind)
BuildRequires:  pkgconfig(cairo) > 1.12.0
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gsl)
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(xmlrpc)
BuildRequires:  pkgconfig(xmlrpc_util)
BuildRequires:  pkgconfig(xmlrpc_client)
BuildRequires:  pkgconfig(json-c)
BuildRequires:  kernel-headers
BuildRequires:  pkgconfig(gtk-doc)
BuildRequires:  python3-docutils

%description
igt-gpu-tools (formally known as intel-gpu-tools) is the standard for writing
test cases for DRM drivers. It also includes a handful of useful tools for
various drivers, such as Intel's GPU tools for i915.

%package docs
Summary:        Documentation for igt-gpu-tools

%description docs
Documentation package for igt-gpu-tools.

%prep
%autosetup -c -p1

%build
# TODO: we don't build overlay quite yet, since /usr/bin/leg isn't packaged
# anywhere. Don't think anyone cares though :)
%meson \
        -Dbuild_man=true \
        -Dbuild_docs=true \
        -Dbuild_audio=true \
        -Dbuild_chamelium=true \
        -Dbuild_runner=true \
        -Dbuild_tests=true \
        -Dwith_libdrm=auto \
        -Dwith_libunwind=false \
        -Dwith_valgrind=true
%meson_build

%install
%meson_install

%check
%meson_test

%files
%license COPYING
%{_libdir}/intel_aubdump.so
%{_libdir}/libigt.so
%{_libdir}/pkgconfig/intel-gen4asm.pc
%{_libexecdir}/igt-gpu-tools/*
%{_datadir}/igt-gpu-tools/*
%{_bindir}/dpcd_reg
%{_bindir}/igt_*
%{_bindir}/intel-gen4asm
%{_bindir}/intel-gen4disasm
%{_bindir}/intel_aubdump
%{_bindir}/intel_audio_dump
%{_bindir}/intel_backlight
%{_bindir}/intel_bios_dumper
%{_bindir}/intel_display_crc
%{_bindir}/intel_display_poller
%{_bindir}/intel_dp_compliance
%{_bindir}/intel_dump_decode
%{_bindir}/intel_error_decode
%{_bindir}/intel_firmware_decode
%{_bindir}/intel_forcewaked
%{_bindir}/intel_framebuffer_dump
%{_bindir}/intel_gem_info
%{_bindir}/intel_gpu_abrt
%{_bindir}/intel_gpu_frequency
%{_bindir}/intel_gpu_time
%{_bindir}/intel_gpu_top
%{_bindir}/intel_gtt
%{_bindir}/intel_guc_logger
%{_bindir}/intel_gvtg_test
%{_bindir}/intel_infoframes
%{_bindir}/intel_l3_parity
%{_bindir}/intel_lid
%{_bindir}/intel_opregion_decode
%{_bindir}/intel_panel_fitter
%{_bindir}/intel_perf_counters
%{_bindir}/intel_reg
%{_bindir}/intel_reg_checker
%{_bindir}/intel_residency
%{_bindir}/intel_stepping
%{_bindir}/intel_vbt_decode
%{_bindir}/intel_watermark
%{_mandir}/man1/intel_*.1*

%files docs
%{_datadir}/gtk-doc/html/igt-gpu-tools/*

%changelog

# vim: expandtab

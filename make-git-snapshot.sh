#!/bin/sh

# Usage: ./make-git-snapshot.sh [COMMIT]
#
# to make a snapshot of the given tag/branch.  Defaults to HEAD.
# Point env var REF to a local mesa repo to reduce clone time.

DIRNAME=igt-gpu-tools-$( date +%Y%m%d )

echo REF ${REF:+--reference $REF}
echo DIRNAME $DIRNAME
echo HEAD ${1:-HEAD}

rm -rf $DIRNAME

git clone ${REF:+--reference $REF} \
	https://gitlab.freedesktop.org/drm/igt-gpu-tools.git $DIRNAME

export GIT_DIR=$DIRNAME/.git

sed -i "igt-gpu-tools.spec" \
	-e "s/%define gitcommit [0-9a-f]\+/%define gitcommit $(git rev-parse HEAD)/" \
	-e "s/%define gitdate [0-9]\+/%define gitdate $(date +%Y%m%d)/"

git archive --format=tar ${1:-HEAD} | bzip2 > $DIRNAME.tar.bz2

# rm -rf $DIRNAME
